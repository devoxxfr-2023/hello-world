//const fs = require('fs')
import { readFileSync, writeFileSync } from 'fs'

//const args = process.argv.slice(2);
//let moduleName = args[0]

//const jsonString = readFileSync('./modsurfer-payload.json').toString();
//const modsurferSmells = JSON.parse(jsonString);

function generateUniqueID() {
    let uniqueID = Math.random().toString(36).substr(2, 8);
    return uniqueID;
}

let getNewGitLabSmell = (description, severity, path) => {
    return {
        engine_name: "modsurfer",
        fingerprint: generateUniqueID(),
        description: description,
        severity: severity,
        location: {
            path: path,
            lines: {
                begin: 0
            }
        }
    }
}

let gitLabSmells = []

// info, minor, major, critical, or blocker
let getSeverity = severity => {
    if(severity < 5) return "info"
    if(severity < 10) return "minor"
    if(severity < 15) return "major"
    if(severity < 20) return "critical"
    return "blocker"
}

gitLabSmells.push(getNewGitLabSmell(
    "nothing too important",getSeverity(4), "..."
))

gitLabSmells.push(getNewGitLabSmell(
    "you should pay attention to this",getSeverity(12), "..."
))


writeFileSync('./gl-code-quality-report.json', JSON.stringify(gitLabSmells))

if(gitLabSmells.length>0) {
  console.log("🟥:", gitLabSmells.length, "smell(s) detected 😡")
  //process.exit(1)
} else {
  console.log("🟩: everithing is fine 🤗")
  //process.exit(0)
}
