import { readFileSync, writeFileSync } from 'fs'

//const args = process.argv.slice(2);

let failureTestCase = (testName, message) => 
`<testcase classname="test_case" name="${testName}">
	<failure message="${message}"/>
</testcase>`

let successTestCase = (testName) => 
`<testcase classname="test_case" name="${testName}">
</testcase>`

let testSuitesTemplate = (xmlTestCases, tests, failures) => {
	return `<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
	<testsuite name="junit-report" tests="${tests}" failures="${failures}" errors="0" skipped="0">
	${xmlTestCases}
	</testsuite>
</testsuites>
`
}

var testCases = []
var tests = 0
var failures = 0

async function testCaseFunction(testName, testCase) {

    let success = testCase()

	tests+=1

	if(success) {
		testCases.push(successTestCase(`Test of ${testName}`))
	} else {
		testCases.push(failureTestCase(`Test of ${testName}`, `😡 Call of ${testName}`))
		failures+=1
	}	
}


await testCaseFunction("Test 1", () => {
	if(5 == 5) {
		return true
	} else {
		return false
	}
})

await testCaseFunction("Test 2", () => {
	if("salut"=="hello") {
		return true
	} else {
		return false
	}
})


writeFileSync('./test-results.xml', testSuitesTemplate(testCases.join("\n"), tests, failures))

